# bookms

#### 介绍
ssm+layui(jsp)+mysql 实现的图书管理系统

#### 软件架构

spring+springmvc+mybatis+mysql+maven


#### 安装教程

1. 下载的压缩包附带了详细的配置过程，如有问题还可联系我。

#### 使用说明

操作演示可看：
    https://blog.csdn.net/qq_28631165/article/details/89741940


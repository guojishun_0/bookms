/*
Navicat MySQL Data Transfer

Source Server         : test
Source Server Version : 50722
Source Host           : localhost:3306
Source Database       : bookms

Target Server Type    : MYSQL
Target Server Version : 50722
File Encoding         : 65001

Date: 2019-05-01 23:51:41
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for ms_admin
-- ----------------------------
DROP TABLE IF EXISTS `ms_admin`;
CREATE TABLE `ms_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_number` varchar(11) NOT NULL COMMENT '登录号',
  `admin_pwd` varchar(255) DEFAULT NULL COMMENT '登录密码',
  `admin_name` varchar(255) DEFAULT NULL COMMENT '名称',
  `login_pre_time` date DEFAULT NULL COMMENT '上次登录时间',
  `del_flg` int(1) DEFAULT NULL COMMENT '是否删除(标记用)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ms_admin
-- ----------------------------
INSERT INTO `ms_admin` VALUES ('1', '1101', '123456', 'Bob', '2019-04-12', '1');
INSERT INTO `ms_admin` VALUES ('2', '1102', '123456', '大天狗', '2019-04-09', '1');
INSERT INTO `ms_admin` VALUES ('3', '1103', '123456', '酒吞童子', '2019-01-09', '1');

-- ----------------------------
-- Table structure for ms_book
-- ----------------------------
DROP TABLE IF EXISTS `ms_book`;
CREATE TABLE `ms_book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL COMMENT '书名',
  `ISBN` varchar(255) DEFAULT NULL COMMENT '统一使用07年新颁布的13位数字',
  `author` varchar(255) DEFAULT NULL COMMENT '作者',
  `introduction` varchar(255) DEFAULT NULL COMMENT '简介',
  `price` varchar(255) DEFAULT NULL COMMENT '价格',
  `publish_time` varchar(255) DEFAULT NULL COMMENT '出版时间',
  `category_id` int(10) DEFAULT NULL COMMENT '类别',
  `image` varchar(255) DEFAULT NULL COMMENT '图片url',
  `create_time` date DEFAULT NULL COMMENT '上架时间',
  `create_admin` varchar(255) DEFAULT NULL COMMENT '上架管理员',
  `update_pre_admin` varchar(255) DEFAULT NULL COMMENT '上一次修改信息的管理员',
  `del_flg` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=344 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ms_book
-- ----------------------------
INSERT INTO `ms_book` VALUES ('1', '活着', '9787506365437', '余华', '《活着》是作家余华的代表作之一，讲述了在大时代背景下，随着内战、三反五反，大跃进，文化大革命等社会变革，徐福贵的人生和家庭不断经受着苦难，到了最后所有亲人都先后离他而去，仅剩下年老的他和一头老牛相依为命。', '25.30', '1923年6月', '1', '', '2019-04-13', 'Bob', '大天狗', '1');

-- ----------------------------
-- Table structure for ms_category
-- ----------------------------
DROP TABLE IF EXISTS `ms_category`;
CREATE TABLE `ms_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL COMMENT '类别号',
  `category_name` varchar(255) DEFAULT NULL COMMENT '类别名称',
  `del_flg` int(1) DEFAULT NULL COMMENT '0表示已删除，1表示未删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ms_category
-- ----------------------------
INSERT INTO `ms_category` VALUES ('1', '1', '小说', '1');
INSERT INTO `ms_category` VALUES ('2', '2', '文学', '1');
INSERT INTO `ms_category` VALUES ('3', '3', '动漫', '1');
INSERT INTO `ms_category` VALUES ('4', '4', '文化', '1');
INSERT INTO `ms_category` VALUES ('5', '5', '传记', '1');
INSERT INTO `ms_category` VALUES ('6', '6', '艺术', '1');
INSERT INTO `ms_category` VALUES ('7', '7', '童书', '1');
INSERT INTO `ms_category` VALUES ('8', '8', '古籍', '1');
INSERT INTO `ms_category` VALUES ('9', '9', '历史', '1');
